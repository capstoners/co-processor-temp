#include "jetson_comms.h"
#include "usart.h"
#include "robot_move.h"
#include "stm32f4xx_usart.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "queue.h"
#include "task.h"

void process_jetson_cmd(char command);
void jestonUSARTReadTask(void * pvParameters);


void jetson_comms_init(){
	BaseType_t ret;

	ret = xTaskCreate(&jestonUSARTReadTask, "lulz2", 1000, NULL, tskIDLE_PRIORITY+4, NULL);
	if(ret != pdPASS){
		configASSERT(0);
	}
}

//needs to be passed ref to usart2
void jestonUSARTReadTask(void * pvParameters){
	char cur;
	BaseType_t retRTOS;
	motor_controller_init();
	for(;;){
		//wait until there is data on Q
//		retRTOS = xQueueReceive(USART1Q, &cur, portMAX_DELAY);
		if(retRTOS == errQUEUE_EMPTY){
			configASSERT(0);
		}
		process_jetson_cmd(cur);
	}
}


void process_jetson_cmd(char command){
	switch(command){
		case 0x1:
			drive_forward(40);
			break;
		case 0x2:
			full_stop();
			break;
		case 0x4:
			turn_right_forever(30);
			break;
		case 0x3:
			turn_left_forever(30);
			break;
	}
}
