
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "encoder.h"



static uint8_t right_track_dir = 0;
static uint8_t left_track_dir = 0;

uint32_t rightFilterFrequency = 0;
uint32_t leftFilterFrequency = 0;

uint8_t get_right_track_dir(){
	return right_track_dir;
}

uint8_t get_left_track_dir(){
	return left_track_dir;
}


void encoder_init(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_ICInitTypeDef  TIM_ICInitStructure; //PWM in
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	// Set up GPIO Inits for encoder pins
	// Left Encoder Phase 1 init struct; for timer 4
	GPIO_InitStructure.GPIO_Pin   = LEFT_PHASE1_PIN;  //PB7
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP ;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_PinAFConfig(LEFT_PHASE1_PORT, LEFT_PHASE1_AF, GPIO_AF_TIM4); // Left phase better be B
	// Right Encoder Phase 1 init struct; for timer 3
	GPIO_InitStructure.GPIO_Pin   = RIGHT_PHASE1_PIN; // PC7
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_PinAFConfig(RIGHT_PHASE1_PORT, RIGHT_PHASE1_AF, GPIO_AF_TIM3);
	// Left Encoder Phase 2 init struct; for timer 4
	GPIO_InitStructure.GPIO_Pin   = LEFT_PHASE2_PIN; //PC6
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
	GPIO_Init(LEFT_PHASE2_PORT, &GPIO_InitStructure);
	// Right Encoder Phase 2 init struct; for timer 3
	GPIO_InitStructure.GPIO_Pin   = RIGHT_PHASE2_PIN; //PB6
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
	GPIO_Init(RIGHT_PHASE2_PORT, &GPIO_InitStructure);

	// Set up and enable NVIC for encoder interrupt pins
	// Timer 3 (right encoder)
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//Timer 4 (left encoder)
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn; // Cole now thinks this is probably right.
	NVIC_Init(&NVIC_InitStructure);

	//Set up Timer parameters for Timers 3 and 4
	// Left Encoder Phase 1 (Timer 4, Channel 2)
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;			// Latch on rising edge
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;		// IC1 give channel 1 info
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;				// prescaler 1; latch on every edge
	TIM_ICInitStructure.TIM_ICFilter = 0x0;

	TIM_ICInit(TIM4, &TIM_ICInitStructure);
	// no idea, but they're reading like < 10% of the actual.
	TIM_SelectInputTrigger(TIM4, TIM_TS_TI2FP2);
	TIM_SelectInputTrigger(TIM3, TIM_TS_TI2FP2); // yolo; we're gonna try this and see if it works.

	//Right Encoder Phase 1 (Timer 3, Channel 2)
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
	TIM_ICInit(TIM3, &TIM_ICInitStructure);
/******** PRETTY SURE THE STUFF ABOVE THIS LINE IS GTG **********/

	/* Select the slave Mode: Reset Mode; Key for the CCR to reset to 0 on positive edges; allows nice math for period between edges. */
	TIM_SelectSlaveMode(TIM4, TIM_SlaveMode_Reset);
	TIM_SelectMasterSlaveMode(TIM4,TIM_MasterSlaveMode_Enable);

	TIM_SelectSlaveMode(TIM3, TIM_SlaveMode_Reset);
	TIM_SelectMasterSlaveMode(TIM3,TIM_MasterSlaveMode_Enable);

	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	TIM_ITConfig(TIM4, TIM_IT_CC2, ENABLE);
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	TIM_ITConfig(TIM3, TIM_IT_CC2, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
	TIM_Cmd(TIM3, ENABLE);

	// "Application" specific (IMD) clock setup to allow for calculations to work within our desired ranges.
//		TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	/* Compute the prescaler value */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);

	TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Prescaler = 280;  // According to math, this should be 128 ish but experimentally wow 280, such a great value.
	// Above line dictates minimum frequency readable.
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV4;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	//No changes between.
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

}


// Right encoder IRQ handler
void TIM3_IRQHandler(void)
{
	static uint32_t rightPreviousFrequency = 0;

	uint32_t rightIC1Value = 0;
	uint32_t rightCurrentFrequency = 0;


	uint8_t right_phase2_status = 0;

	uint8_t overrunOccured = 0;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	/* Clear TIM3 Capture compare interrupt pending bit */


	if(TIM_GetITStatus(RIGHT_TIMER, TIM_IT_Update) == 1 && TIM_GetITStatus(RIGHT_TIMER, TIM_IT_CC2) == 0)
	{
		TIM_ClearITPendingBit(RIGHT_TIMER, TIM_IT_Update);
		rightCurrentFrequency = 0;
		overrunOccured = 1;
	}else if(TIM_GetITStatus(RIGHT_TIMER, TIM_IT_Update)==1)
	{
		TIM_ClearITPendingBit(RIGHT_TIMER, TIM_IT_Update);
		return;
	}

	TIM_ClearITPendingBit(RIGHT_TIMER, TIM_IT_CC2);



	/* Get the Input Capture value */
	rightIC1Value = TIM_GetCapture2(RIGHT_TIMER);

	// read state of phase 2 pin to get direction ***VERIFY THESE ASSIGNMENTS***
	right_phase2_status = GPIO_ReadInputDataBit(RIGHT_PHASE2_PORT, RIGHT_PHASE2_PIN);
	if (right_phase2_status == 0)
	{
		right_track_dir = FORWARD;
	}
	else
	{
		right_track_dir = REVERSE;
	}

	// compute the frequency pin
	if (rightIC1Value != 0 || overrunOccured)
	{

		//DutyCycle = (TIM_GetCapture1(TIM3) * 1000) / IC2Value;
		//Frequency computation
		//TIM4 counter clock = (RCC_Clocks.HCLK_Frequency)/2
		if(!overrunOccured)
		{
		rightCurrentFrequency = (RCC_Clocks.HCLK_Frequency)/ COUNT_TO_HZ / rightIC1Value;
		}else
			overrunOccured = 0;
				if (rightCurrentFrequency >= THRESHOLD)
				{
					rightCurrentFrequency = rightPreviousFrequency;
				}
			}
			else
			{
				//DutyCycle = 0;
				rightCurrentFrequency = 0;
			}
			// calculate running average frequency ***is this a bad idea for when we change speed or will it reflect well enough?
			rightFilterFrequency = 0.7*rightCurrentFrequency + 0.3*rightFilterFrequency;
			rightPreviousFrequency = rightCurrentFrequency;

}



void TIM4_IRQHandler(void)
{
	static uint32_t leftPreviousFrequency = 0;

	uint32_t leftIC1Value = 0;
	uint32_t leftCurrentFrequency = 0;

	uint8_t left_phase2_status = 0;

	uint8_t overrunOccured = 0;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	/* Clear TIM3 Capture compare interrupt pending bit */


	if(TIM_GetITStatus(LEFT_TIMER, TIM_IT_Update) == 1 && TIM_GetITStatus(LEFT_TIMER, TIM_IT_CC2) == 0)
	{
		TIM_ClearITPendingBit(LEFT_TIMER, TIM_IT_Update);
		leftCurrentFrequency = 0;
		overrunOccured = 1;
	}else if(TIM_GetITStatus(LEFT_TIMER, TIM_IT_Update)==1)
	{
		TIM_ClearITPendingBit(LEFT_TIMER, TIM_IT_Update);
		return;
	}

	TIM_ClearITPendingBit(LEFT_TIMER, TIM_IT_CC2);



	/* Get the Input Capture value */
	leftIC1Value = TIM_GetCapture2(LEFT_TIMER);

	// read state of phase 2 pin to get direction ***VERIFY THESE ASSIGNMENTS***
	left_phase2_status = GPIO_ReadInputDataBit(LEFT_PHASE2_PORT, LEFT_PHASE2_PIN);
	if (left_phase2_status == 0)
	{
		left_track_dir = FORWARD;
	}
	else
	{
		left_track_dir = REVERSE;
	}

	// compute the frequency pin
	if (leftIC1Value != 0 || overrunOccured)
	{

		//DutyCycle = (TIM_GetCapture1(TIM3) * 1000) / IC2Value;
		//Frequency computation
		//TIM4 counter clock = (RCC_Clocks.HCLK_Frequency)/2
		if(!overrunOccured)
		{
		leftCurrentFrequency = (RCC_Clocks.HCLK_Frequency)/ COUNT_TO_HZ / leftIC1Value;
		}else
			overrunOccured = 0;
				if (leftCurrentFrequency >= THRESHOLD)
				{
					leftCurrentFrequency = leftPreviousFrequency;
				}
			}
			else
			{
				//DutyCycle = 0;
				leftCurrentFrequency = 0;
			}
			// calculate running average frequency ***is this a bad idea for when we change speed or will it reflect well enough?
			leftFilterFrequency = 0.7*leftCurrentFrequency + 0.3*leftFilterFrequency;
			leftPreviousFrequency = leftCurrentFrequency;

}
