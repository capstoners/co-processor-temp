#include "stm32f4xx.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "croutine.h"
#include "led.h"
#include "can.h"
#include "usart.h"
#include "robot_move.h"
#include "myTasks.h"
#include "speedFeedback.h"


extern uint32_t ADC_Val;

static uint8_t leftPowerCmd;
static uint8_t rightPowerCmd;
void myFirstTask(void * pvParameters){
	uint8_t data = 0x55;
	for(;;){
		led_toggle();

		vTaskDelay(pdMS_TO_TICKS(250));

	}
	vTaskDelete( NULL );
}


void mySecondTask(void * pvParameters){
	motor_controller_init();

	for(;;){
		//robot_test();

		// deal with when state machine is defined
		/*
		if (ADC_Val < THRESH) {
			setSetPoint(150);
		} else setSetPoint(150);
		// end state machine deal with
		*/

		leftPowerCmd = leftSpeedFeedback();
		drive_forward_l(leftPowerCmd);
		vTaskDelay(10);
		rightPowerCmd = rightSpeedFeedback();
		//rightPowerCmd = leftPowerCmd;
		//rightPowerCmd = 50;
		drive_forward_r(rightPowerCmd);

//		drive_forward2(leftPowerCmd,rightPowerCmd);
		vTaskDelay(10);

	}
	vTaskDelete( NULL );
}
