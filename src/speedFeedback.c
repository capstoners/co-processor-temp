/*
 * speedFeedback.c
 *
 *  Created on: Nov 14, 2017
 *      Author: Matt Kehler, Keeley Edwards
 */
#include "stm32f4xx.h"

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "myTasks.h"
#include "usart.h"
#include "encoder.h"
#include "robot_move.h"
#include "speedFeedback.h"

static float setPoint = DEFAULT_SET_POINT;
static float leftError = 0;
static float leftSetPoint;
static uint8_t leftPowerCommandActual = 0;
static int leftPowerCommandConversion = 0;
static float leftPowerCommand;
static float leftErrorIntegral = 0;

static float rightError = 0;
static float rightSetPoint;
static uint8_t rightPowerCommandActual = 0;
static int rightPowerCommandConversion = 0;
static float rightPowerCommand;
static float rightErrorIntegral = 0;

// Need to figure out how to abstract these / make global
extern uint32_t leftFilterFrequency;
extern uint32_t rightFilterFrequency;
extern USART_t stlink;

// need to add implementation for left and right tracks independently

/* Computes new power command to send to motor controller.
 * Software implementation of PI controller.
 * Returns new power command.
 */
uint8_t leftSpeedFeedback() {
	leftSetPoint = setPoint;

	leftError = leftSetPoint/350.0 - leftFilterFrequency/350.0;
	leftErrorIntegral += leftError;

	// Signal filtering (normalized)
	leftPowerCommand = 0.8*leftError + 0.1*leftErrorIntegral;
	//powerCommand = 0.9*error + 0.2*errorIntegral;
	//powerCommand = 0*error + 0.02*errorIntegral;

	leftPowerCommandConversion = leftPowerCommand * 127;

	if(leftPowerCommandConversion>127)
		leftPowerCommandActual = 127;
	else if(leftPowerCommandConversion>0)
		leftPowerCommandActual = (uint8_t)(leftPowerCommandConversion&0xFF);
	else
		leftPowerCommandActual = 0;

	//usartPutf(&stlink, (float)setPoint, "setFreq %f\r");
	usartPutf(&stlink, (float)leftFilterFrequency, "freq %f\n");
	return leftPowerCommandActual;
}

uint8_t rightSpeedFeedback() {
	rightSetPoint = setPoint;

	rightError = rightSetPoint/350.0 - rightFilterFrequency/350.0;
	rightErrorIntegral += rightError;

	// Signal filtering (normalized)
	rightPowerCommand = 0.8*rightError + 0.1*rightErrorIntegral;
	//powerCommand = 0.9*error + 0.2*errorIntegral;
	//powerCommand = 0*error + 0.02*errorIntegral;

	rightPowerCommandConversion = rightPowerCommand * 127;

	if(rightPowerCommandConversion>127)
		rightPowerCommandActual = 127;
	else if(rightPowerCommandConversion>0)
		rightPowerCommandActual = (uint8_t)(rightPowerCommandConversion&0xFF);
	else
		rightPowerCommandActual = 0;

	//usartPutf(&stlink, (float)setPoint, "setFreq %f\r");
	usartPutf(&stlink, (float)rightFilterFrequency, "freq %f\n");
	return rightPowerCommandActual;
}

/* Assigned value to "setPoint" based on a new set point from outside speed feedback.
 * Can be used to change the speed set point of the motor in response to, for example,
 * ADC or Jetson commands.
 */

void setSetPoint(float newSetPoint){
	if(newSetPoint > MAX_SET_POINT)
		setPoint = MAX_SET_POINT;
	else if(newSetPoint>0)
		setPoint = newSetPoint;
	else
		setPoint = 0;		//do we want to set to default set point instead?
}

