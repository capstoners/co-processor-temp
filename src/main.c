/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f4xx.h"


#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "croutine.h"
#include "myTasks.h"
#include "usart.h"
#include "led.h"
#include "can.h"
#include "robot_test_open_loop.h"
#include "encoder.h"
#include "ADC.h"

//#include "jetson_comms.h"


USART_t stlink;
uint32_t ADC_Val;
int main(void)
{
	encoder_init();
	led_init();
	stLinkUsartInit(&stlink);
	canInit();
	DMAforADC1Config(&ADC_Val);
	ADC_Config();


	//motor_controller_init();
	//jetson_usart_init(&my_jetson_usart);
	//jetson_comms_init();
	BaseType_t ret = 0;
	ret = xTaskCreate(
			&myFirstTask, 		//pointer to the task function
			"myTask", 			//name for task
			1000, 				//stack size, will not run if it is too small
			NULL, 				//pvparameters, what you pass to the task
			tskIDLE_PRIORITY+3, //task priority
			NULL 				//task handle
			);
	if(ret != pdPASS){
		for(;;); //loop forever
	}

	ret = xTaskCreate(
			&mySecondTask, 		//pointer to the task function
			"myTask2", 			//name for task
			10000, 				//stack size, will not run if it is too small
			NULL, 				//pvparameters, what you pass to the task
			tskIDLE_PRIORITY+2, //task priority
			NULL 				//task handle
			);
	if(ret != pdPASS){
		for(;;); //loop forever
	}

	//starts the schedule, tasks start running
	vTaskStartScheduler();


}

//will get called on stack overflow
void vApplicationStackOverflowHook(void)
{
	configASSERT(0);
}

