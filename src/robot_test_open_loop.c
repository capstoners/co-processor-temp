/*
 * robot_test_open_loop.c
 *
 *  Created on: Oct 5, 2017
 *      Author: Keeley Edwards
 *
 *  Written to test basic robot functionality:
 *
 *  Drive forward
 *  Wait .5 sec
 *  Drive in reverse
 *  Wait .3 sec
 *  Turn Left
 *  Wait .7 sec
 *  Drive forward
 *  Wait .1 sec
 *  Turn Right
 *  Stop
 */


#include "stm32f4xx.h"
#include "robot_move.h"
#include "packetized_serial.h"
#include "robot_test_open_loop.h"
#include "FreeRTOS.h"
#include "task.h"
#include "usart.h"

void robot_test(){
	full_stop();
	drive_forward(40);
	vTaskDelay(pdMS_TO_TICKS(1000));
	full_stop();
	vTaskDelay(pdMS_TO_TICKS(500));
	drive_reverse(40);
	vTaskDelay(pdMS_TO_TICKS(1000));
	full_stop();
	vTaskDelay(pdMS_TO_TICKS(500));
	turn_left(40,90);
	vTaskDelay(pdMS_TO_TICKS(500));
	turn_right(40,90);
	full_stop();
	vTaskDelay(pdMS_TO_TICKS(1000));
}
