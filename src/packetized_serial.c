/*
 * packetized_serial.c
 *
 *  Created on: Oct 11, 2017
 *      Author: parallels
 */
#include "stm32f4xx.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include <stdint.h>
#include "packetized_serial.h"
USART_t mc_usart;

void usart_init(){
	// call usart init
	mcUsartInit(&mc_usart);
	}

void serial_packet(serial_command_t command, uint8_t speed, uint8_t address){

	usartPutc(&mc_usart,address);
	usartPutc(&mc_usart,command);
	usartPutc(&mc_usart,speed);
	usartPutc(&mc_usart,(address+command+speed)&127);

}
