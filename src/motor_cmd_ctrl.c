/*
 * motor_cmd_ctrl.c
 *
 *  Created on: Oct 5, 2017
 *      Author: Keeley Edwards
 *
 *  Pseudo low-level motor functions.
 *  motor_init() initializes the motors and stops the robot
 *
 *  motor_simple(track, direction, speed) sets the basic value to send to the motor controller over serial.
 *  The controller accepts the following values from 0-255 to drive a specific command:
 *
 *  0 -> stop
 *	1-63 -> right track forward
 *	63-127 -> right track reverse
 *	128-190 -> left track forward
 *	191-255 -> left track reverse
 *
 *	full_stop() this function sends a 0 to the motor controller to stop both tracks simultaneously.
 */

#include "stm32f4xx.h"
#include "motor_cmd_ctrl.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
USART_t mc_usart;

void motor_init(){

	// call usart init
	mcUsartInit(&mc_usart);
	// initialize both tracks to stop;
	}
	
void motor_simple(enum track_select track, enum track_direction direction, uint8_t speed){
	uint8_t speed_cmd;
	/*
	switch (track) {
	case RIGHT_TRACK:
		switch (direction) {
		case FORWARD:
			// normalize speed b/w 1-63
			speed_cmd = RIGHT_FWD_OFFSET + speed;
			break;
		case REVERSE:
			// normalize speed b/w 64-127
			speed_cmd = RIGHT_REV_OFFSET + speed;
			break;
		default:
			speed_cmd = 0;
		}
		break;
	case LEFT_TRACK:
		switch (direction) {
		case FORWARD:
			// normalize speed b/w 128-190
			speed_cmd = LEFT_FWD_OFFSET + speed;
			break;
		case REVERSE:
			// normalize speed b/w 191-255
			speed_cmd = LEFT_REV_OFFSET + speed;
			break;
		default:
			speed_cmd = 0;
		}
		break;
	default:
		speed_cmd = 0;
	}*/
	// pass speed_cmd value over serial
	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,14);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,20);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(130+14+20)&127);

	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(130+0+0)&127);

	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,4);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(130+4+0)&127);

	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,50);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(130+0+50)&127);

	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,5);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,50);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(130+5+50)&127);
	vTaskDelay(pdMS_TO_TICKS(5000));

	//usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	//usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	//usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	//usartPutc(&mc_usart,(0+0+130)&127);

	/*
	usartPutc(&mc_usart,130);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,4);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,0);
	//vTaskDelay(pdMS_TO_TICKS(170));
	usartPutc(&mc_usart,(0+4+130)&127);
*/
}

/*
void full_stop(){
	usartPutc(&mc_usart,0);
}*/
