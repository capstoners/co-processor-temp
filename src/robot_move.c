/*
 * robot_move.c
 *
 *  Created on: Oct 5, 2017
 *      Author: Keeley Edwards
 *
 *  Defines messages to send to motor controller for basic robot movement:
 *
 *  drive_forward(speed) activates both tracks to move "forward" at the same speed
 *  drive_reverse(speed) activates both tracks to move "in reverse" at the same speed
 *  turn_left(max_speed, turn_angle) moves right motor "forward" and the left "in reverse" to turn the robot left
 *  turn_right(max_speed, turn_angle) moves right motor "forward" and the left "in reverse" to turn the robot right
 *
 *	Units:
 *	Speeds are given in m/s.
 *	Angles are given in degrees.
 *	Time is in milliseconds.
 */

#include "stm32f4xx.h"
#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "croutine.h"
#include "robot_move.h"
#include "packetized_serial.h"

void motor_controller_init(){
	usart_init();
	serial_packet(TIMEOUT, 10, TRACKS_ADDR);
}
void full_stop(){
	serial_packet(LEFT_FORWARD, 0, TRACKS_ADDR);
	serial_packet(RIGHT_FORWARD, 0, TRACKS_ADDR);
}
void drive_forward_l(uint8_t speed){
	// open loop control
	serial_packet(LEFT_FORWARD, speed, TRACKS_ADDR);
}

void drive_forward(uint8_t speed){
	serial_packet(LEFT_FORWARD, speed, TRACKS_ADDR);
	serial_packet(RIGHT_FORWARD, speed, TRACKS_ADDR);
}

void drive_forward2(uint8_t speed_l, uint8_t speed_r){
	serial_packet(LEFT_FORWARD, speed_l, TRACKS_ADDR);
	serial_packet(RIGHT_FORWARD, speed_r, TRACKS_ADDR);
}

void drive_forward_r(uint8_t speed){
	serial_packet(RIGHT_FORWARD, speed, TRACKS_ADDR);
}

void drive_reverse(uint8_t speed){
	// open loop control
	serial_packet(LEFT_REVERSE, speed, TRACKS_ADDR);
	serial_packet(RIGHT_REVERSE, speed, TRACKS_ADDR);
}

void turn_left(uint8_t max_speed, uint8_t turn_angle){
	uint32_t turning_time;
	serial_packet(RIGHT_FORWARD, max_speed, TRACKS_ADDR);
	serial_packet(LEFT_REVERSE, max_speed, TRACKS_ADDR);
	turning_time = angle_to_time(turn_angle);
	vTaskDelay(pdMS_TO_TICKS(turning_time));
	full_stop();
}

void turn_right(uint8_t max_speed, uint8_t turn_angle){
	uint32_t turning_time;
	serial_packet(LEFT_FORWARD, max_speed, TRACKS_ADDR);
	serial_packet(RIGHT_REVERSE, max_speed, TRACKS_ADDR);
	turning_time = angle_to_time(turn_angle);
	vTaskDelay(pdMS_TO_TICKS(turning_time));
	full_stop();
}

void turn_left_forever(uint8_t max_speed){
	//full_stop();
	//vTaskDelay(pdMS_TO_TICKS(500));
	serial_packet(RIGHT_FORWARD, max_speed, TRACKS_ADDR);
	serial_packet(LEFT_REVERSE, max_speed, TRACKS_ADDR);
}
void turn_right_forever(uint8_t max_speed){
	//full_stop();
	//vTaskDelay(pdMS_TO_TICKS(500));
	serial_packet(LEFT_FORWARD, max_speed, TRACKS_ADDR);
	serial_packet(RIGHT_REVERSE, max_speed, TRACKS_ADDR);
}

uint32_t angle_to_time(uint8_t turn_angle){
	uint32_t turning_time;
	turning_time = TURN_RATE * turn_angle; // time to delay in ms
	return turning_time;
}
