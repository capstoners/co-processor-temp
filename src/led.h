#ifndef LED_H
#define LED_H

#define NUCLEO_LED_PIN 		GPIO_Pin_5
#define NUCLEO_LED_PORT 	GPIOA

void led_init();
void led_toggle();

#endif
