#ifndef MOTOR_CMD_CTRL_H
#define MOTOR_CMD_CTRL_H
#include "stm32f4xx.h"


enum track_select{
	LEFT_TRACK = 0,
	RIGHT_TRACK
};


enum track_direction{
	FORWARD = 0,
	REVERSE
};

#define RIGHT_FWD_OFFSET 1
#define RIGHT_REV_OFFSET 65
#define LEFT_FWD_OFFSET 128
#define LEFT_REV_OFFSET 193

void motor_init();
void motor_simple(enum track_select track, enum track_direction direction, uint8_t speed);
//void full_stop();

#endif
