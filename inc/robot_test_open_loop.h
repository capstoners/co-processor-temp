/*
 * robot_test_open_loop.h
 *
 *  Created on: Oct 5, 2017
 *      Author: parallels
 */

#ifndef ROBOT_TEST_OPEN_LOOP_H
#define ROBOT_TEST_OPEN_LOOP_H

#include "stm32f4xx.h"

void robot_test();
#endif /* ROBOT_TEST_OPEN_LOOP_H_ */
