/*
 * packetized_serial.h
 *
 *  Created on: Oct 11, 2017
 *      Author: parallels
 */

#ifndef PACKETIZED_SERIAL_H_
#define PACKETIZED_SERIAL_H_
#include "stm32f4xx.h"
#include "motor_cmd_ctrl.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include <stdint.h>

typedef enum serial_command{
	RIGHT_REVERSE = 0,
	RIGHT_FORWARD,
	MIN_VOLTAGE,
	MAX_VOLTAGE,
	LEFT_REVERSE,
	LEFT_FORWARD,
	RIGHT_7_BIT,
	LEFT_7_BIT,
	FORWARD_MIXED,
	BACKWARD_MIXED,
	RIGHT_TURN_MIXED,
	LEFT_TURN_MIXED,
	FWD_BACK_7_BIT,
	TURN_7_BIT,
	TIMEOUT,
	BAUD_RATE,
	RAMPING,
	DEADBAND
}serial_command_t;

#define TRACKS_ADDR 130
#define FLIPPER_ADDR 128

void serial_packet(serial_command_t command, uint8_t speed, uint8_t address);
void usart_init();


#endif /* PACKETIZED_SERIAL_H_ */
