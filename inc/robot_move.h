/*
 * robot_move.h
 *
 *  Created on: Oct 5, 2017
 *      Author: Keeley Edwards
 */

#ifndef ROBOT_MOVE_H
#define ROBOT_MOVE_H
#include "stm32f4xx.h"

void motor_controller_init();
void drive_forward(uint8_t speed);
void drive_forward2(uint8_t speed_l, uint8_t speed_r);
void drive_forward_l(uint8_t speed);
void drive_forward_r(uint8_t speed);
void drive_reverse(uint8_t speed);
void turn_left(uint8_t max_speed, uint8_t turn_angle);
void turn_right(uint8_t max_speed, uint8_t turn_angle);
void turn_left_forever(uint8_t max_speed);
void turn_right_forever(uint8_t max_speed);
void full_stop();
uint32_t angle_to_time(uint8_t angle);

#define TURN_RATE 30	// ms / degree

#endif /* ROBOT_MOVE_H_ */
