/*
 * speedFeedback.h
 *
 *  Created on: Nov 14, 2017
 *      Author: parallels
 */

#ifndef SPEEDFEEDBACK_H_
#define SPEEDFEEDBACK_H_

uint8_t leftSpeedFeedback();
uint8_t rightSpeedFeedback();
void setSetPoint(float newSetPoint);

#define MAX_SET_POINT 350
#define DEFAULT_SET_POINT 100
#endif /* SPEEDFEEDBACK_H_ */
