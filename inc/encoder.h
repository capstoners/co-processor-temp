/*
 * encoder.h
 *
 *  Created on: Oct 26, 2017
 *      Author: Keeley Edwards
 */

#ifndef ENCODER_H_
#define ENCODER_H_

extern int DCG2;

/*function prototypes ---*/
void encoder_init(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
uint8_t get_right_track_dir(void);
uint8_t get_left_track_dir(void);

// Set binary value for determined track direction
#define FORWARD 0
#define REVERSE 1

#define COUNT_TO_HZ 562

// Pin identifiers for encoder phases
#define LEFT_PHASE1_PIN		GPIO_Pin_7
#define LEFT_PHASE1_AF 		GPIO_PinSource7
#define LEFT_PHASE1_PORT 	GPIOB

#define LEFT_PHASE2_PIN		GPIO_Pin_6
#define LEFT_PHASE2_PORT	GPIOB

#define RIGHT_PHASE1_PIN	GPIO_Pin_7
#define RIGHT_PHASE1_AF		GPIO_PinSource7
#define RIGHT_PHASE1_PORT	GPIOC

#define RIGHT_PHASE2_PIN	GPIO_Pin_6
#define RIGHT_PHASE2_PORT	GPIOC

#define RIGHT_TIMER TIM3
#define LEFT_TIMER TIM4

#define THRESHOLD 600	// set maximum reasonable frequency of encoders


#endif /* MAIN_H_ */

