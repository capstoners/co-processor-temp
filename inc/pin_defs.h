#ifndef PIN_DEFS_H
#define PIN_DEFS_H


/************* PORTA *********************/
//PA0
//Signal Name: Rad Feedback
//Config: Analog


/************* PORTB *********************/

//PB6
//Signal Name: MC USART TX
//Config: AF (USART)
#define JETSON_USART_TX_PIN 				GPIO_Pin_6
#define JETSON_USART_TX_AFPIN 				GPIO_PinSource6
#define JETSON_USART_TX_PORT 				GPIOB

//PB7
//Signal Name: MC USART RX
//Config: AF (USART)
#define JETSON_USART_RX_PIN 				GPIO_Pin_7
#define JETSON_USART_RX_AFPIN 				GPIO_PinSource7
#define JETSON_USART_RX_PORT 				GPIOB


//PB10
//Signal Name: MC USART RX
//Config: AF (USART)
#define MOTOR_CONTROL_USART_TX_PIN 			GPIO_Pin_10
#define MOTOR_CONTROL_USART_TX_AFPIN 		GPIO_PinSource10
#define MOTOR_CONTROL_USART_TX_PORT 		GPIOB


/************* PORTC *********************/

//PC5
//Signal Name: MC USART RX
//Config: AF (USART)
#define MOTOR_CONTROL_USART_RX_PIN 			GPIO_Pin_5
#define MOTOR_CONTROL_USART_RX_AFPIN 		GPIO_PinSource5
#define MOTOR_CONTROL_USART_RX_PORT 		GPIOC


/************* PORTD *********************/


/************* PORTE *********************/

#endif
